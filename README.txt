THIS IS MY LIBRARY FOR MORPHING IMAGES.

Some Important Software Requirements:
1. X11 (Xquartz on Mac)
2. Boost and Boost-Python
3. Dlib

For Mac:
https://www.learnopencv.com/install-dlib-on-macos/

For Ubuntu:
https://www.learnopencv.com/install-dlib-on-ubuntu/

For Windows:
https://www.learnopencv.com/install-opencv-3-and-dlib-on-windows-python-only/

Usage:
Pass the following command-line arguments to __init__.py in morphing/ folder.
python3 __init__.py <face/body> <path_to_first_image> <path_to_second_image> <duration_of_video_to_be_created> <frame_rate> <path_where_video_is_to_be_saved>